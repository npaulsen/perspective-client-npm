Welcome to Perspective Feature Management!
======================
Perspective is a cloud SaaS tool designed to simplify much of the boilerplate
code for settings and feature management. This bit of code was one of the first
things I'd write every time I started a new project, so I decided to move it to 
the cloud and create a simple npm plugin to make settings management a few minute
task. This client is the npm module for connecting to your cloud instance of 
Perspective. You can sign up at https://tamaton.com/products/perspective

Perspective provides a broad feature set, but it is primarily focused on the 
following:

- Managing environment specific variable values.
- Feature management for turning functions on/off without code deployments
- Complex object and ruleset management for modifying application behavior without code deployments
- Endpoint management across suite of applications, simplifying network mapping and updating connections


Getting Started
===============

First, make sure you have signed up at https://tamaton.com/products/perspective

Next, log in to https://portal.tamaton.com to get your account and project keys.

Add this module to your project.

Create an instance of the Perspective object using the static Perspective.instantiate method.

You can see usage, examples and additional documentation at ...


Thank you for using Perspective!

The Tamaton Team
https://tamaton.com
