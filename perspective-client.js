"use strict";

const https = require('https');

//default interval = 5 min
const DEFAULT_INTERVAL = 5 * 60 * 1000;
const INTERVAL_OVERRIDE_SETTING = 'perspective-cache-time';

class Perspective {
    
    /**
     * 
     * @param {type} config =
     * 
     * {
     *      accountKey: {
     *          accountId: "12345678-1234-1234-1234-123456789abc", 
     *          email: "your.account@email.com" 
     *      },
     *     apiKey: "12345678-1234-1234-1234-123456789abc", 
     *     application: "app-name", 
     *     environment: "env" 
     *  }
     * 
     * @param {type} interval
     * @param {type} instance
     * @return {Perspective}
     */
    constructor(config, interval, instance) {
        this._cache = '';
        
        if (typeof interval !== 'number') {
          throw new TypeError('The "interval" property is required and must be of type number.');
        }
        
        this._interval = interval;
        
        if (typeof config !== 'object' || config === null) {
          throw new TypeError('The "config" argument is required and must be of type Object.');
        }

        if (typeof config.accountKey !== 'object' || config.accountKey === null) {
          throw new TypeError('The "config.accountKey" property is required and must be of type Object.');
        }

        if (typeof config.accountKey.accountId !== 'string') {
          throw new TypeError('The "config.accountKey.accountId" property is required and must be of type string.');
        }

        if (typeof config.accountKey.email !== 'string') {
          throw new TypeError('The "config.accountKey.email" property is required and must be of type string.');
        }

        if (typeof config.apiKey !== 'string') {
          throw new TypeError('The "config.apiKey" property is required and must be of type string.');
        }

        if (typeof config.application !== 'string') {
          throw new TypeError('The "config.application" property is required and must be of type string.');
        }

        this._data = JSON.stringify(config);
        
        if (instance === null || instance === undefined || instance === '') {
            instance = '';
        }else{
            instance = instance+'.';
        }
        
        this._options = {
            hostname: instance+'services.tamaton.com',
            timeout: 5000,
            port: 443,
            path: '/perspective/api/get-parameters.do',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic YmFzaWMtdXNlcjohbjB0aGluZy10MC1zMzMtaGVyZSE='
            }
        };

        this.updateCache();

        this._timer = setInterval(this.runtimeSafeUpdateCache.bind(this),interval);
    }
    
    static instantiate(config){
        return new Perspective(config, DEFAULT_INTERVAL, null);
    }
    
    static instantiateWithInterval(config, interval){
        return new Perspective(config, interval, null);
    }
    
    static instantiateWithInstance(config, interval, instance){
        return new Perspective(config, interval, instance);
    }
    
    runtimeSafeUpdateCache(){
        try{
            var update = this.updateCache.bind(this);
            update();
        }catch (error) {
            console.error(error);
        }
    }
    
    updateCache(){
        var req = https.request(this._options, res => {
            console.log('statusCode: '+res.statusCode);

            var output = '';
            res.on('data', chunk => {
                output += chunk;
            });
            res.on('end', () => {
                console.debug('cache refresh: '+output);
                this._cache = JSON.parse(output);
                var check = this.checkTimer.bind(this);
                check();
            });
        });

        req.on('error', error => {
          console.error(error);
        });

        req.write(this._data);
        req.end();
    }
    
    checkTimer(){
        var timer = this.get(INTERVAL_OVERRIDE_SETTING);
        console.debug('timer: '+timer);
        if(timer !== undefined && timer !== null && timer !== this._interval){
            this._interval = timer;
            
            clearInterval(this._timer);
            this._timer = setInterval(this.updateCache.bind(this),this._interval);
        }
    }
    
    get(key){
        var val = this._cache[key];

        if(val === undefined){
            return undefined;
        }else if(val === null){
            return null;
        }else{
            return this._cache[key].value;
        }
    }
    
}

module.exports = Perspective;
